#!/bin/bash
set -e

apt-get update
apt install -y docker.io

# install binenv
wget -q https://github.com/devops-works/binenv/releases/download/v0.19.0/binenv_linux_amd64
wget -q https://github.com/devops-works/binenv/releases/download/v0.19.0/checksums.txt
sha256sum  --check --ignore-missing checksums.txt
mv binenv_linux_amd64 binenv
chmod +x binenv
./binenv update
./binenv install binenv
rm binenv
if [[ -n $BASH ]]; then ZESHELL=bash; fi
if [[ -n $ZSH_NAME ]]; then ZESHELL=zsh; fi
echo $ZESHELL
echo -e '\nexport PATH=~/.binenv:$PATH' >> ~/.${ZESHELL}rc
echo "source <(binenv completion ${ZESHELL})" >> ~/.${ZESHELL}rc

# install kind
binenv install kind
binenv install kubectl
kind create cluster --name madcon-staging --config kind-config.yaml --image kindest/node:v1.29.10

# install metallb (https://kind.sigs.k8s.io/docs/user/loadbalancer/#apply-metallb-manifest)
kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml

# wait till metallb is ready
kubectl wait --namespace metallb-system \
                --for=condition=ready pod \
                --selector=app=metallb \
                --timeout=90s

# 
# get first two octets of docker network
FIRST_TWO_OCTETS=$(docker network inspect -f '{{.IPAM.Config}}' kind |  grep -oE '\b([0-9]{1,3}\.){3}[0-9]{1,3}\b' | head -n 1 |  awk -F'.' '{print $1 "." $2}')

cat >./metallb-config.yaml <<EOF
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: metallb-pool
  namespace: metallb-system
spec:
  addresses:
  - ${FIRST_TWO_OCTETS}.255.200-${FIRST_TWO_OCTETS}.255.250
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: empty
  namespace: metallb-system
EOF

kubectl apply -f ./metallb-config.yaml


./binenv install sops

# create flux-system before adding sops secret for decryption
kubectl create namespace flux-system
echo "Create sops-gpg secret in flux-system namespace with gpg secret key for decryption of secrets."
echo 'Extract secret key on kmadacs notebook: export KEY_FP=84F30EA309D32574484D6423A92F7AAEA0075F8C; gpg --export-secret-keys --armor "${KEY_FP}"'
echo "or take if from password manager and store it to ~/secret.key and run cat ~/secret.key | kubectl create secret generic sops-gpg --namespace=flux-system --from-file=sops.asc=/dev/stdin"
echo "-----"
echo "Create ssh private/public key secret in k8s cluster in order to fluxcd can access github account"
echo "Secret yaml is stored encrypted by sops in k8s/secrets/ssh-flux-secret.yaml"
echo "You can decrypt it with sops command sops  --decrypt ssh-flux-secret.yaml > ssh-flux-secret-clear.yaml"
echo "and apply it to k8s cluster kubectl apply -f ssh-flux-secret-clear.yaml"

# bootstrap flux on cluster
flux bootstrap git --url=ssh://git@gitlab.com/madcon/k8s.git --branch=staging --path=clusters/staging --private-key-file ~/.ssh/id_rsa
